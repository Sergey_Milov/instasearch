package test.com.instasearch.Services;

import java.util.List;

import retrofit.Call;

import retrofit.http.GET;
import retrofit.http.Query;
import test.com.instasearch.Models.InstaResponse;


public interface MediaService {
        @GET("/v1/users/704791681/media/recent")
        Call<InstaResponse> getMedia(@Query("client_id") String clientId);

        @GET("/v1/users/704791681/media/recent/")
        Call<InstaResponse> getMediaNext(@Query("client_id") String clientId, @Query("max_id") String maxId);

}