package test.com.instasearch.Services;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;


public class ServiceGenerator {
    public static final String BASE_URL = "https://api.instagram.com";

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    public static  <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.build();

        return retrofit.create(serviceClass);
    }

}
