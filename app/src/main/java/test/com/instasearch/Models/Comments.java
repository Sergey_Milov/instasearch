package test.com.instasearch.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Comments {

    public int count;

    public int getCount() {
        return count;
    }

    @SerializedName("data")
    public List<CommentData> dataComments;

    public List<CommentData> getDataComments() {
        return dataComments;
    }
}
