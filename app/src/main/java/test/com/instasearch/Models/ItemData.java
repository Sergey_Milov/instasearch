package test.com.instasearch.Models;


import com.google.gson.annotations.SerializedName;

public class ItemData {

    @SerializedName("caption")
    public  Caption mCaption;

    public  Caption getCaption() {
        return mCaption;
    }

    public Images images;
    public Images getImages() {
        return images;
    }

    public Likes likes;

    public Likes getLikes() {
        return likes;
    }

    public Comments comments;

    public Comments getComments() {
        return comments;
    }
}
