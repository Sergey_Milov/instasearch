package test.com.instasearch.Models;

import com.google.gson.annotations.SerializedName;


public class CommentData {

    @SerializedName("text")
    public String commentText;

    public String getCommentText() {
        return commentText;
    }

    @SerializedName("from")
    public Follower follower;

    public Follower getFollower() {
        return follower;
    }
}
