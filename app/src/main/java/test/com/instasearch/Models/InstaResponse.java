package test.com.instasearch.Models;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class InstaResponse {

    @SerializedName("data")
    public List<ItemData> mData;

    @SerializedName("pagination")
    public NextDataUrl nextDataUrl;

    public NextDataUrl getNextDataUrl(){
        return nextDataUrl;
    }

    public List<ItemData> getmData(){
        return mData;
    }



}
