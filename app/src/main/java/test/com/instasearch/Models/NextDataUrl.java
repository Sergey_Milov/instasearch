package test.com.instasearch.Models;

import com.google.gson.annotations.SerializedName;


public class NextDataUrl {

    @SerializedName("next_max_id")
    public String nextIdUrl;

    public String getNextIdUrl() {
        return nextIdUrl;
    }
}
