package test.com.instasearch.Models;

import com.google.gson.annotations.SerializedName;


public class Images {

    @SerializedName("standard_resolution")
    public TargetImage image;

    public TargetImage getImage() {
        return image;
    }
}
