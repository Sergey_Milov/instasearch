package test.com.instasearch;

import android.os.Bundle;
import android.os.Handler;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.Log;

import android.view.Menu;
import android.view.MenuItem;


import java.util.List;

import retrofit.Call;
import retrofit.Callback;

import retrofit.Response;
import retrofit.Retrofit;

import test.com.instasearch.Adapters.MyAdapter;
import test.com.instasearch.Models.InstaResponse;
import test.com.instasearch.Models.ItemData;
import test.com.instasearch.Services.MediaService;
import test.com.instasearch.Services.OnLoadMoreListener;
import test.com.instasearch.Services.ServiceGenerator;


public class MainActivity extends AppCompatActivity implements Callback<InstaResponse>, OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener{

    private RecyclerView recVieew;
    private LinearLayoutManager mLayoutManager;

    private MyAdapter adapter;
    private String a;
    private static final String TAG = "myLogs";
    protected Handler handler;
    private List<ItemData> data;
    private InstaResponse reaponceData;
    private MediaService service;
    private Callback<InstaResponse> callback;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean swiping;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        handler = new Handler();
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener(this);

        recVieew = (RecyclerView) findViewById(R.id.recView);
        //mLayoutManager = new GridLayoutManager(this, 3);
        mLayoutManager = new LinearLayoutManager(this);
        recVieew.setLayoutManager(mLayoutManager);
        recVieew.setHasFixedSize(true);

        service = ServiceGenerator.createService(MediaService.class);
        createCall(service.getMedia(getResources().getString(R.string.client_id)));


    }



    public void createCall(Call<InstaResponse> call) {

        call.enqueue(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResponse(final Response<InstaResponse> response, Retrofit retrofit) {

        int statusCode = response.code();
        if (statusCode == 200) {
            reaponceData = response.body();
            if (data == null) {
                data = reaponceData.getmData();
                adapter = new MyAdapter(data, getApplicationContext(), recVieew);
                recVieew.setAdapter(adapter);
                adapter.setOnLoadMoreListener(this);
            } else {
                if (swiping) {

                    data.clear();
                    data.addAll(reaponceData.getmData());
                    adapter.notifyDataSetChanged();
                    //adapter.updateComments();
                    swiping = false;
                } else {

                    data.addAll(reaponceData.getmData());
                    adapter.notifyItemInserted(data.size());
                }
                adapter.setLoaded();
            }
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onFailure(Throwable t) {

    }

    @Override
    public void onLoadMore() {
        Log.d(TAG, String.valueOf(data.size()) + " elements");
        data.add(null);
        Log.d(TAG, String.valueOf(data.size()) + " elements");
        adapter.notifyItemInserted(data.size() - 1);


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                data.remove(data.size() - 1);
                Log.d(TAG, data.size() + " after removing");
                adapter.notifyItemRemoved(data.size());
                createCall(service.getMediaNext(getResources().getString(R.string.client_id),
                        reaponceData.getNextDataUrl().getNextIdUrl()));

            }
        }, 2000);
    }

    @Override
    public void onRefresh() {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                swiping = true;
                //service = ServiceGenerator.createService(MediaService.class);
                createCall(service.getMedia(getResources().getString(R.string.client_id)));

            }
        }, 2000);


    }
}
