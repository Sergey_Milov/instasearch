package test.com.instasearch.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import test.com.instasearch.Models.ItemData;
import test.com.instasearch.R;


/**
 * Created by imac1 on 09/11/15.
 */
public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder>{

    private ArrayList<ItemData> mListData;
    private int currentPost;

    public CommentsAdapter(ArrayList<ItemData> data, int currentPost){
        this.currentPost = currentPost;
        mListData = data;
    }

    @Override
    public CommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentsAdapter.ViewHolder holder, int position) {
          holder.textView.setText(mListData.get(currentPost).getComments().getDataComments().get(position).getCommentText());
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            //textView = (TextView) itemView.findViewById(R.id.tvComment);
        }
    }

    public void setCurrentPost(int currentPost){
        this.currentPost = currentPost;
    }

}
