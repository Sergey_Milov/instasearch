
package test.com.instasearch.Adapters;

import android.app.Application;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import test.com.instasearch.Models.CommentData;
import test.com.instasearch.Models.ItemData;
import test.com.instasearch.R;
import test.com.instasearch.Services.OnLoadMoreListener;

public class MyAdapter extends RecyclerView.Adapter{
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private RecyclerView.ViewHolder vh;

    private static final String TAG = "myLogs";
    private ArrayList<ItemData> mListData;
    public Context context;
    private LayoutInflater layoutInflater;

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView mTextView;
        public ImageView mImageView;
        public RecyclerView mRvComments;
        public TextView mTvLikes;
        public Context context;
        public ArrayList<ItemData> mListData;
        public int currentPost = 0;
        public LinearLayout llItem;

        //public ArrayList<CommentData> commentList;
        //public CommentsAdapter commentsAdapter;

        public ViewHolder(View v, ViewGroup parent, LayoutInflater layoutInflater) {
            super(v);


            mTextView = (TextView) v.findViewById(R.id.tvItem);
            mImageView = (ImageView) v.findViewById(R.id.ivItem);
            mTvLikes = (TextView) v.findViewById(R.id.tvLikes);

            llItem = (LinearLayout) v.findViewById(R.id.llItem);

        }
    }

    @Override
    public int getItemViewType(int position) {
        /*Log.d(TAG, String.valueOf(mListData.get(position) != null ? VIEW_ITEM : VIEW_PROG)
                + " type of view holder");*/
        return mListData.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public MyAdapter(List<ItemData> myDataset, Context context, RecyclerView recyclerView) {
        mListData = (ArrayList<ItemData>) myDataset;
        this.context = context;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                .getLayoutManager();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                Log.d(TAG, String.valueOf(recyclerView.getAdapter().getItemCount()));

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (lastVisibleItem == 117) {
                    Log.d(TAG, "ololo");
                }
                if (!loading
                        && totalItemCount <= (lastVisibleItem + visibleThreshold)) {

                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();

                    }
                    loading = true;
                }
            }
        });
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {

        //RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_layout, parent, false);


            vh = new ViewHolder(v, parent, layoutInflater);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {

            ((ViewHolder) holder).mTextView.setText(mListData.get(position).getCaption().getText());
            Picasso.with(context).load(mListData.get(position).getImages().getImage().getUrl()).fit().centerCrop().into(((ViewHolder) holder).mImageView);
            ((ViewHolder) holder).mTvLikes.setText("Нравится: " + mListData.get(position).getLikes().getCount());

            addComment(position, holder);

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded() {
        loading = false;
    }

    public void addComment(int position, RecyclerView.ViewHolder holder) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        if (((ViewHolder) holder).llItem.getChildCount() == 3) {
            for (int i = 0; i <= 4; i++) {
                TextView v = (TextView) LayoutInflater.from(context).inflate(R.layout.item_comment_layout, null);
                v.setText(mListData.get(position).getComments().getDataComments().get(i).getFollower().getUsername()
                        + ": " + mListData.get(position).getComments().getDataComments().get(i).getCommentText());
                v.setMaxLines(3);
                v.setTextColor(Color.BLACK);
                v.setLayoutParams(layoutParams);
                v.setId(i);

                ((ViewHolder) holder).llItem.addView(v);
            }
        } else {
            for (int i = 0; i <= 4; i++) {
                TextView v = (TextView) ((ViewHolder) holder).llItem.findViewById(i);
                v.setText(mListData.get(position).getComments().getDataComments().get(i).getFollower().getUsername()
                        + ": " + mListData.get(position).getComments().getDataComments().get(i).getCommentText());

            }

        }
    }

    /*public void updateComments(){
        for (int i = 0; i <= 4; i++) {
            TextView v = (TextView) ((ViewHolder) vh).llItem.findViewById(i);
            v.setText(mListData.get(position).getComments().getDataComments().get(i).getFollower().getUsername()
                    + ": " + mListData.get(position).getComments().getDataComments().get(i).getCommentText());

        }

    }*/

}


